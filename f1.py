import logging


from Tools.scripts.which import msg


def log(msg):
    logging.basicConfig(filename="example_log",level=logging.DEBUG, format="%(asctime)s - %(message)s")
    logging.debug(msg)
if __name__=="__main__":
    log(msg)