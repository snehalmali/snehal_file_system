import hashlib


# create class for file operation:
class file_operations:

    def __init__(self, file_path):
        self.file_path = file_path

    #  open a file and reading text from file
    def read_file(self):
        with open(self.file_path, "r")as fp:
            contents = fp.read()
            print(contents)

    # if os.path.isfile(self.file_path):
    #     print("File exists and is readable")

    # else:
    #     print(self.file_path, "\nFile not found, sorry")

    # open and write on text fi
    def write_file(self):
        with open(self.file_path, "w") as fp:
            contents = fp.write('this is sample text file')
            print(contents)

    # open and append the data on text file

    def append_file(self):
        with open(self.file_path, "a+") as fp:
            contents = fp.write("\nthis is appended text \n appended text2 \n appended text3")
            print(contents)

    # read the contents of  file line by line

    def read_line_by_line(self):
        with open(self.file_path, "r+")as file:
            text = file.readlines()
            for line in text:
                print(line)

    # create checksum of text file

    def checksum(self):
        hasher = hashlib.md5()
        with open(self.file_path, "rb")as open_file:
            content = open_file.read()
            hasher.update(content)
        print('checksum of file=', hasher.hexdigest())


a = file_operations(r"D:\Pycharm Project\S_Project1\test.txt")
# a.read_file()
# a.write_file()
# a.append_file()
# a.read_line_by_line()
a.checksum()
