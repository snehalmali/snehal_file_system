import logging
import glob
LOG_FILENAME = 'newfile.log'

logging.basicConfig(filename=LOG_FILENAME, level=logging.DEBUG, format='%(asctime)s%(message)s', filemode='w')

logging.debug('this is debug message')
logging.info('this is info message')
logging.warning('This is warning message')
logging.error('This is error message')
logging.critical('This is critical error message')
LEVELS = {
    'debug': logging.DEBUG, 'info': logging.INFO, 'warning': logging.WARNING, 'error': logging.ERROR,
    'critical': logging.CRITICAL, }
newfile= glob.glob('%s' % LOG_FILENAME)
for filename in newfile:
    print(filename)
