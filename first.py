import os, sys, stat
path = "D:\Pycharm Project\\myfile.txt"

#open a file
fd = os.open(path, os.O_RDWR)
#reading text
ret = os.read(fd, os.stat("myfile.txt").st_size)
print(ret.decode())
#number of bytes written to myfile.txt
numBytes = os.write(fd, ret)
print(" Number of bytes written:", numBytes)

#close opened file
os.close(fd)
print("closed the file Sucessfully..")

# writing text
str = "This is new text file"
line = str.encode()
print(str, line)
print("decoded str=", str)