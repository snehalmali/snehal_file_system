def read():  # open a text file for reading data

    fd = open("test.txt", "r")
    if fd.mode == "r":
        contents = fd.read()
        print(contents)
        fd.close()
        print("text file closed sucessfully")


class file_system:
    pass


def write():
    fd = open("test.txt", "w+")
    for i in range(1):
        fd.write("Python is an interpreted, object-oriented "
                 "\nhigh-level programming language with "
                 "\ndynamic semantics.")
        # number of bytes written to test.txt
        print(" Number of bytes written:", fd.write)
        fd.close()


write()


def read_line_by_line():
    fd = open("test.txt", "r+")
    # read file line by line
    f1 = fd.readlines()
    for x in f1:
        print(x)


read_line_by_line()


def append():
    fd = open("test.txt", "a+")
    # write some lines of data to the file
    for i in range(3):
        fd.write("\n appended line %d\r\n" % (i + 1))
        # close the file
    fd.close()


append()
file_system()
